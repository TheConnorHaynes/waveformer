package com.mygdx.game;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.files.FileHandle;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.utils.Array;
import com.badlogic.gdx.utils.Json;
import com.badlogic.gdx.utils.JsonValue;

import static com.mygdx.game.MainGame.itemSheet;

/**
 * Created by Connor on 5/24/2016.
 */
public class Door extends Tile{
    public String locationFile;
    public Vector2 destination;

    public Door(int x, int y, int sx, int sy, boolean flipx, boolean flipy){
        super(x, y, sx, sy, flipx, flipy);
        locationFile = "level.json";
        destination = new Vector2(64,64);
        texture = itemSheet[sx][sy];
        height = 40;
        width = 40;


    }

    public Door(){
        super();
        texture = itemSheet[sx][sy];
        height = 40;
        width = 40;
    }

    public void read(Json json, JsonValue jsonData){
        destination = new Vector2(jsonData.getInt("destination.x"), jsonData.getInt("destination.y"));
        locationFile = jsonData.getString("locationFile");
        super.read(json, jsonData);
        if(sx < itemSheet.length && sy < itemSheet[sx].length)
            texture = itemSheet[sx][sy];
        else texture = itemSheet[0][0];
        height = 40;
        width = 40;
    }

    public void write(Json json){
        json.writeValue("destination.x", destination.x);
        json.writeValue("destination.y", destination.y);
        json.writeValue("locationFile", locationFile);
        super.write(json);
    }

    public void interact(){
        MainGame.b++;
        FileHandle loc;
        loc = Gdx.files.internal("StaticWorld/"+locationFile);
        if(loc.exists()){
            MainGame.world = MainGame.json.fromJson(World.class, loc);
            MainGame.world.player.x = destination.x;
            MainGame.world.player.y = destination.y;
            MainGame.world.player.spawn = destination;
        }
        else {
            MainGame.world = new World();
        }
        MainGame.m = Gdx.files.internal("StaticWorld/"+locationFile);
        MainGame.n = loc;
    }

    public Door clone(int x, int y, boolean fx, boolean fy){
        Door c = new Door(x, y, sx, sy, fx, fy);
        return c;
    }


}
