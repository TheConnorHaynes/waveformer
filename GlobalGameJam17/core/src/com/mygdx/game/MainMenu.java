package com.mygdx.game;

import com.badlogic.gdx.*;
import com.badlogic.gdx.audio.Music;
import com.badlogic.gdx.audio.Sound;
import com.badlogic.gdx.files.FileHandle;
import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.GL20;
import com.badlogic.gdx.graphics.OrthographicCamera;
import com.badlogic.gdx.graphics.Pixmap;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.BitmapFont;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.graphics.g2d.TextureAtlas;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.badlogic.gdx.graphics.g3d.Shader;
import com.badlogic.gdx.graphics.glutils.FrameBuffer;
import com.badlogic.gdx.graphics.glutils.ShaderProgram;
import com.badlogic.gdx.graphics.glutils.ShapeRenderer;
import com.badlogic.gdx.math.Vector3;
import com.badlogic.gdx.scenes.scene2d.InputEvent;
import com.badlogic.gdx.scenes.scene2d.Stage;
import com.badlogic.gdx.scenes.scene2d.ui.Skin;
import com.badlogic.gdx.scenes.scene2d.ui.Table;
import com.badlogic.gdx.scenes.scene2d.ui.TextButton;
import com.badlogic.gdx.scenes.scene2d.utils.ClickListener;
import com.badlogic.gdx.utils.Json;
import com.badlogic.gdx.utils.viewport.*;

public class MainMenu implements Screen{
    Texture logo;

    @Override
    public void resize(int width, int height) {
        viewport.update(width, height);
        camera.position.set(camera.viewportWidth / 2, camera.viewportHeight / 2, 0);
        camera.update();
    }

    @Override
    public void pause(){

    }

    @Override
    public void resume(){

    }

    @Override
    public void hide(){

    }

    @Override
    public void dispose() {
        skin.dispose();
        atlas.dispose();
    }

    private boolean controls;
    private boolean credits;

    private SpriteBatch batch;
    protected Stage stage;
    private Viewport viewport;
    private OrthographicCamera camera;
    private TextureAtlas atlas;
    protected Skin skin;
    private Music music;

    private int choice;

    private BitmapFont font;

    @Override
    public void show() {
        logo = new Texture(Gdx.files.internal("hank goes home.png"));
        controls = false;
        credits = false;
        font = new BitmapFont(Gdx.files.internal("font/8-BIT_WONDER.fnt"), Gdx.files.internal("font/8-BIT_WONDER_0.png"), false);
        music = Gdx.audio.newMusic(Gdx.files.internal("_GameJam_2_.wav"));
        music.setLooping(true);
        music.play();

        batch = new SpriteBatch();
        camera = new OrthographicCamera();
        viewport = new FitViewport(800, 600, camera);
        viewport.apply();

        camera.position.set(camera.viewportWidth / 2, camera.viewportHeight / 2, 0);
        camera.update();
        choice = 0;
    }


    @Override
    public void render(float delta) {
        Gdx.gl.glClearColor(.1f, .12f, .16f, 1);
        Gdx.gl.glClear(GL20.GL_COLOR_BUFFER_BIT);

        batch.begin();
        if(credits){
            font.setColor(Color.WHITE);
            font.draw(batch, "Lead Programmer:    Connor Haynes", 40, 450);
            font.draw(batch, "Composer:                  Ahmed Mian", 40, 350);
            font.draw(batch, "Pixel Artist:              Elliott Imlay", 40, 300);
            font.draw(batch, "Programmer:             James Jackman", 40, 400);
            font.draw(batch, "Level Designer:         Ken Weu", 40, 250);
        }
        else if(controls){

            font.setColor(Color.WHITE);
            font.draw(batch, "Right to move Right", 40, 450);
            font.draw(batch, "Left to move left", 40, 400);
            font.draw(batch, "Z to jump           as in zebra", 40, 350);
            font.draw(batch, "X to interact     as in xylophone", 40, 300);
        }
        else {
            batch.draw(logo, 300, 250);
            if (choice == 0)
                font.setColor(Color.GOLD);
            else
                font.setColor(Color.WHITE);
            font.draw(batch, "Play", 40, 300);
            if (choice == 1)
                font.setColor(Color.GOLD);
            else
                font.setColor(Color.WHITE);
            font.draw(batch, "Controls", 40, 200);
            if (choice == 2)
                font.setColor(Color.GOLD);
            else
                font.setColor(Color.WHITE);
            font.draw(batch, "Credits", 40, 100);

        }
        batch.end();
        pollInput();


    }

    public void pollInput(){
        if(Gdx.input.isKeyJustPressed(Input.Keys.ENTER)){
            if(choice == 0){
                System.out.println("this runs");
                ((Game)Gdx.app.getApplicationListener()).setScreen(new MainGame());
            }
            else if(choice == 1){
                if(controls) {
                    font.getData().setScale(1);
                    controls = false;
                }
                else{
                    font.getData().setScale(.5f);
                    controls = true;
                }
            }
            else
                if(credits) {
                    font.getData().setScale(1);
                    credits = false;
                }
                else{
                    font.getData().setScale(.5f);
                    credits = true;
                }
        }

        if(Gdx.input.isKeyJustPressed(Input.Keys.S) || Gdx.input.isKeyJustPressed(Input.Keys.DOWN)){
            choice++;
            if(choice>2)
                choice = 0;
        }

        if(Gdx.input.isKeyJustPressed(Input.Keys.W) || Gdx.input.isKeyJustPressed(Input.Keys.UP)){
            choice--;
            if(choice<0)
                choice = 2;
        }


    }
}
