package com.mygdx.game;

import com.badlogic.gdx.graphics.g2d.SpriteBatch;

import static com.mygdx.game.MainGame.stringTime;

/**
 * Created by hcmg_local on 1/21/2017.
 */
public class StringTile extends Tile{
    float amplitude;
    private boolean vibrate;

    public StringTile(int x, int y, int sx, int sy, boolean fx, boolean fy){
        super(x,y,sx,sy,fx,fy);
        vibrate = false;
        amplitude = -1;
        width = 2;
        height = 1;
    }
    public StringTile(){
        super();
        vibrate = false;
        amplitude = -1;
        width = 2;
        height = 2;
    }

    public void stop(){
        vibrate = false;
        amplitude = -1;
    }

    public void vibrate(StringCapTile t){
        vibrate = true;
        int ty = (int)y/20 *20;
        if(t.y >= ty && t.y< ty+20) {
            float amp = Math.abs(x - t.x) / 2;
            System.out.println(amp);
            if (amplitude == -1 || amp < amplitude) {
                amplitude = amp;
            }
        }

    }

    @Override
    public float getY(MovingObject o){
        if(!vibrate)
            return y;
        else
            return y+(float)Math.sin(o.x/80+MainGame.stringTime)*amplitude;
    }

//    @Override
//    public float getHeight(MovingObject o){
//        if(!vibrate)
//            return height;
//        else
//            return height*(float)Math.abs(Math.sin(o.x/80+MainGame.time));
//    }

    @Override
    public void render(SpriteBatch batch, float delta, World world){
        if(vibrate) {
            batch.setShader(MainGame.sinShader);
            MainGame.sinShader.setUniformf("time", stringTime);
            MainGame.sinShader.setUniformf("amplitude", amplitude);
        }
        batch.draw(texture, x, y, width, height);
        batch.setShader(MainGame.defaultShader);
    }

    public StringTile clone(int x, int y, boolean fx, boolean fy){
        return new StringTile(x, y, sx, sy, fx, fy);
    }
}
