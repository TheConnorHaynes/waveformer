package com.mygdx.game;

import com.badlogic.gdx.utils.Json;
import com.badlogic.gdx.utils.JsonValue;

/**
 * Created by hcmg_local on 1/21/2017.
 */
public class StringCapTile extends Tile {
    public StringCapTile(int x, int y, int sx, int sy, boolean fx, boolean fy){
        super(x,y,sx,sy,fx,fy);
        texture = MainGame.waveSheet[1][0];
    }
    public StringCapTile(){
        super();
        texture = MainGame.waveSheet[1][0];
    }
    public StringCapTile clone(int x, int y, boolean fx, boolean fy){
        return new StringCapTile(x,y,sx,sy,fx,fy);
    }

    public void activateString(World world, boolean vibrate){
        for(Tile t : world.foreground){
            if(t instanceof StringTile){
                if (vibrate) {
                    ((StringTile) t).stop();
                }
                else {
                    ((StringTile) t).vibrate(this);
                }
            }
        }
    }

    @Override
    public void read(Json json, JsonValue jsonData){
        super.read(json, jsonData);
        texture = MainGame.waveSheet[1][0];
    }
}
