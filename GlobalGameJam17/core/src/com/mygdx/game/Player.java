package com.mygdx.game;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.Animation;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.utils.Array;
import com.badlogic.gdx.utils.Json;
import com.badlogic.gdx.utils.JsonValue;

/**
 * Created by Connor on 5/12016.
 */
public class Player extends Creature implements Json.Serializable{
    TextureRegion[][] spriteSheet;
    Animation up;
    Animation down;
    Animation run;
    Animation idle;
    Animation wall;
    Vector2 spawn;
    float time;
    public final int MAX_HEALTH = 20;
    public final float INVINCIBILITY_TIME = 1;
    public float invincibility;

    public Player(){
        super(64, 64, 40, 40);
        invincibility = -1;
        element = 1;
        grounded = false;
        flipped = false;
        animEnd = .8f;
        time = 0;
        spriteSheet = new TextureRegion(new Texture("hank.png")).split((int)width,(int)height);
        up = new Animation(.2f, spriteSheet[1]);
        down = new Animation(.2f, spriteSheet[2]);
        run = new Animation(.1f, spriteSheet[0]);
        idle = new Animation(.2f, spriteSheet[3]);
        health = MAX_HEALTH;


        animation = new Animation(.25f, spriteSheet[0]);
        animation.setPlayMode(Animation.PlayMode.LOOP);
        height = 40;
        width = 20;

        spawn = new Vector2(x,y);

    }

    public Player(float x, float y){
        super(x, y, 40, 40);
        System.out.println("new player");
        invincibility = -1;
        element = 1;
        grounded = false;
        flipped = false;
        animEnd = 1;
        time = 0;
        spriteSheet = new TextureRegion(new Texture("hank.png")).split((int)width,(int)height);
        up = new Animation(.2f, spriteSheet[1]);
        down = new Animation(.2f, spriteSheet[2]);
        run = new Animation(.1f, spriteSheet[0]);
        idle = new Animation(.2f, spriteSheet[3]);
        health = MAX_HEALTH;


        animation = new Animation(.2f, spriteSheet[0]);
        animation.setPlayMode(Animation.PlayMode.LOOP);
        height = 40;
        width = 20;


        spawn = new Vector2(x,y);

    }


    public void updatePhysics(){
        if(invincibility > 0)
            invincibility -= (1.0/60.0);
        if(speed != 0) {
            animEnd = 1;
            time = 0;
            if(time > animEnd)
                time = 0;
            animation = run;
        }
        else{
            animEnd = 1;
            if(time > animEnd)
                time = 0;
            animation = idle;

        }
        if(dy > 0 && !grounded){

            if(time > animEnd)
                time = 0;
            animEnd = 1;
            animation = up;


        }
        else if(dy < -5f && !grounded){

            animEnd = 1;
            animation = idle;
            if(time > animEnd)
                time = 0;
        }




        if((speed > 0) && flipped) {
            for (int i = 0; i < spriteSheet.length; i++)
                for (int j = 0; j < spriteSheet[i].length; j++)
                    spriteSheet[i][j].flip(true, false);
            flipped = false;
        }
        else if(speed < 0 && !flipped){
            for (int i = 0; i < spriteSheet.length; i++)
                for (int j = 0; j < spriteSheet[i].length; j++)
                    spriteSheet[i][j].flip(true, false);
            flipped = true;
        }



        x+=10;
        super.updatePhysics();
        x-=10;

        if(y<0-height)
            kill();
    }


    @Override
    public void kill(){
        x = spawn.x;
        y = spawn.y;
        System.out.println(spawn.x);
        System.out.println(spawn.y);
    }



    public void render(SpriteBatch batch, float delta, World world){
        super.render(batch, delta, world);

    }

    public void hurt(int dmg, boolean knockback){
        if(invincibility < 0){
            health -= dmg;
            invincibility = INVINCIBILITY_TIME;
        }
        if(health <= 0){
            kill();
        }
    }



    @Override
    public void write(Json json) {

    }

    @Override
    public void read(Json json, JsonValue jsonData) {

    }

    public void interact(World world){
        Tile i = new Tile();
        for (Tile t : world.background) {
            if (t.collidex(this) != 0) {

                if (t instanceof SwitchTile) {
                    i = t;
                }
                if(t instanceof Door){
                    i = t;
                }
            }
        }

        if(i instanceof SwitchTile)
            ((SwitchTile) i).interact();
        else if(i instanceof Door)
            ((Door) i).interact();
    }



    public boolean equals(Object o){
        return (o instanceof Player);

    }


}
