package com.mygdx.game;

/**
 * Created by hcmg_local on 1/22/2017.
 */
public class DrumSwitch extends SwitchTile {
    public DrumSwitch(){
        super();
    }

    public DrumSwitch(int x, int y, int sx, int sy, boolean fx, boolean fy){
        super(x,y,sx,sy,fx,fy);
    }

    public void interact(){
        MainGame.activateDrums();
    }

    public DrumSwitch clone(int x, int y, boolean fx, boolean fy){
        return new DrumSwitch(x,y,sx,sy,fx,fy);
    }
}
