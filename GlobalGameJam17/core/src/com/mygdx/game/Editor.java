package com.mygdx.game;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.files.FileHandle;
import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.graphics.glutils.ShapeRenderer;
import com.badlogic.gdx.math.Vector;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.math.Vector3;
import com.badlogic.gdx.utils.Array;

import java.util.Scanner;

/**
 * Created by Connor on 5/15/2016.
 */
public class Editor {
    public int gridSize;
    //give the editor access to the level
    World world;
    //array of tiles to place
    Array<Tile> inventory;
    Vector3 mousepos;

    float first;
    float last;
    //how many fit on the screen
    final float LENGTH = 10;
    //which tile in inv is selected
    int selected;
    //whether or not the texture is flipped
    boolean flipx;
    boolean flipy;

    public Editor(World w){
        world = w;
        gridSize = MainGame.TILE_SIZE;
        inventory = new Array<Tile>();
        selected = 0;
        first = 0;
        last = LENGTH;
        flipx = false;
        flipy = false;

        //add all tile types here
        inventory.add(new Tile(0,0,0,0,false,false));
        inventory.add(new Tile(0,0,0,1,false,false));
        inventory.add(new Tile(0,0,0,2,false,false));
        inventory.add(new Tile(0,0,0,3,false,false));
        inventory.add(new Tile(0,0,0,4,false,false));
        inventory.add(new Tile(0,0,0,5,false,false));
        inventory.add(new Tile(0,0,0,6,false,false));
        inventory.add(new Tile(0,0,0,7,false,false));
        inventory.add(new Tile(0,0,0,8,false,false));
        inventory.add(new Tile(0,0,0,11,false,false));
        inventory.add(new StringTile(0,0,0,11,false,false));
        inventory.add(new StringCapTile(0,0,0,1,false,false));
        inventory.add(new Drum(0,0,0,0,false, false));
        inventory.add(new StringSwitch(0,0,0,10,false,false));
        inventory.add(new DrumSwitch(0,0,0,9,false,false));
        inventory.add(new Door(0,0,0,0,false,false));
        inventory.add(new Door(0,0,1,0,false,false));
        inventory.add(new Door(0,0,2,0,false,false));
        inventory.add(new Door(0,0,3,0,false,false));
        inventory.add(new Door(0,0,4,0,false,false));
        inventory.add(new Door(0,0,5,0,false,false));
        inventory.add(new Door(0,0,6,0,false,false));
        inventory.add(new Door(0,0,7,0,false,false));
        inventory.add(new Door(0,0,8,0,false,false));
        inventory.add(new Door(0,0,9,0,false,false));






    }

    public void changeGridSize(){
        if(gridSize == 20)
            gridSize = 2;
        else
            gridSize = 20;
    }
    public void removeBackground(){
        if(world.background.size>0)
            world.background.pop();
    }

    public void addBackground(){
        Tile t = inventory.get(selected).clone((int) mousepos.x / (gridSize) * (gridSize), (int) mousepos.y / (gridSize) * (gridSize), flipx, flipy);
        if(!world.background.contains(t, false)) {
            world.background.add(inventory.get(selected).clone((int) mousepos.x / (gridSize) * (gridSize), (int) mousepos.y / (gridSize) * (gridSize), flipx, flipy));
        }
    }

    public void render(SpriteBatch batch, ShapeRenderer shapes, float x, float y){
        shapes.setAutoShapeType(true);
        shapes.begin();
        shapes.set(ShapeRenderer.ShapeType.Line);
        shapes.setColor(Color.WHITE);
        for(int i = (int)x; i < x + 800; i++)
            if(i%(gridSize) == 0)
                shapes.line(i, (int)y, i, (int)y + 600);

        for(int i = (int)y; i < y+600; i++)
            if(i%(gridSize) == 0)
                shapes.line((int)x, i, (int)x + 800, i);

        shapes.setColor(Color.YELLOW);



        shapes.rect((int) mousepos.x / (gridSize) * (gridSize), (int) mousepos.y / (gridSize) * (gridSize), (gridSize), (gridSize));

        shapes.setColor(Color.WHITE);
        shapes.set(ShapeRenderer.ShapeType.Filled);
        shapes.rect(x + 800 / 8 * 7, y, 800 / 8, 600);
        shapes.setColor(Color.RED);
        shapes.set(ShapeRenderer.ShapeType.Line);
        shapes.rect(800 * 15 / 16 + x - 1, 600 - 80 - (selected - first) * 40 + y - 1, 41, 41);
        if(flipx)
            shapes.line(800 + x - 10, 600 + y - 5, 800 + x, 600 +y -5);
        if(flipy)
            shapes.line(800 + x - 5, 600 + y - 10, 800 + x-5, 600 +y);
        shapes.end();
        batch.begin();
        for(int i = (int)first; i <= last; i++){
            if(i < inventory.size && i >= 0)
                inventory.get(i).render(batch, 800*15/16+x, 600 - 80 - (i-first)*40+y);
        }
        batch.end();

    }

    public void setMouse(Vector3 m){
        mousepos = m;
    }

    public void addForeground(){
        Tile t = inventory.get(selected).clone((int) mousepos.x / (gridSize) * (gridSize), (int) mousepos.y / (gridSize) * (gridSize), flipx, flipy);
        if(!world.foreground.contains(t, false)) {
            world.foreground.add(inventory.get(selected).clone((int) mousepos.x / (gridSize) * (gridSize), (int) mousepos.y / (gridSize) * (gridSize), flipx, flipy));
        }
    }



    public void removeForeground(){
        if(world.foreground.size>0)
            world.foreground.pop();
    }



    public void clearTile(){
        for(Tile t : world.foreground){
            if(t.x == (int)mousepos.x/(gridSize)*(gridSize) &&  t.y == (int)mousepos.y/(gridSize)*(gridSize))
                world.foreground.removeValue(t, false);
        }

        for(Tile t : world.background){
            if(t.x == (int)mousepos.x/(gridSize)*(gridSize) &&  t.y == (int)mousepos.y/(gridSize)*(gridSize))
                world.background.removeValue(t, false);
        }
    }
    public void scroll(int s){
        selected += s;
        if(selected < 0)
            selected = 0;
        if(selected >= inventory.size)
            selected = inventory.size - 1;
        if(selected < first){
            first--;
            last--;
        }
        if(selected > last){
            first++;
            last++;
        }
    }

    public void flipx(){
        if(flipx)
            flipx = false;
        else
            flipx = true;
    }

    public void flipy(){
        if(flipy)
            flipy = false;
        else
            flipy = true;
    }

    public void setDoorLevel(){

    }
}
