package com.mygdx.game;

import com.badlogic.gdx.Input;

/**
 * Created by Connor on 5/26/2016.
 */
public class SwingBox implements Input.TextInputListener {

    @Override
    public void input(String text) {
        MainGame.nextLevel = text;
    }

    @Override
    public void canceled() {
    }
}
