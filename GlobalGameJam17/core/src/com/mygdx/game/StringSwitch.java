package com.mygdx.game;

/**
 * Created by hcmg_local on 1/22/2017.
 */
public class StringSwitch extends SwitchTile{
    public StringSwitch(){
        super();
    }
    public StringSwitch(int x, int y, int sx, int sy, boolean fx, boolean fy){
        super(x,y,sx,sy,fx,fy);
    }

    public StringSwitch clone(int x, int y, boolean fx, boolean fy){
        return new StringSwitch(x,y,sx,sy,fx,fy);
    }

    public void interact(){
        MainGame.activateStrings();
        MainGame.stringTime = 0;
    }
}
