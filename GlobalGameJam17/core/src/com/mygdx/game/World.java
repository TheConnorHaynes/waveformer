package com.mygdx.game;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.utils.Array;
import com.badlogic.gdx.utils.Json;
import com.badlogic.gdx.utils.JsonValue;

/**
 * Created by Connor on 5/10/2016.
 */
public class World{
    public Array<Tile> foreground;
    public Array<Tile> background;
    public Array<MovingObject> moving;
    public Player player;
    public float time;


    public World(){
        time = 0;
        player = new Player();
        foreground = new Array<Tile>();
        background = new Array<Tile>();
        moving = new Array<MovingObject>();
        foreground.add(new Tile(80,0,0,0,false,false));
    }

    public void updatePhysics(){
        for(MovingObject o : moving)
            o.updatePhysics();
        player.updatePhysics();
    }

    public void render(SpriteBatch batch, float delta){
        for(int i = 0; i < background.size; i++)
            background.get(i).render(batch, delta, this);
        batch.end();
        batch.begin();
        player.render(batch, delta, this);
        for(MovingObject o : moving)
            o.render(batch, delta, this);
        for(Tile tile : foreground)
            tile.render(batch, delta, this);
    }

    public World clone(){
        World r = new World();
        r.foreground = foreground;
        r.background = background;
        return r;
    }


}
