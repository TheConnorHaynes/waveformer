package com.mygdx.game;

import com.badlogic.gdx.Audio;
import com.badlogic.gdx.Game;
import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Input;
import com.badlogic.gdx.Screen;
import com.badlogic.gdx.audio.Music;
import com.badlogic.gdx.audio.Sound;
import com.badlogic.gdx.files.FileHandle;
import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.GL20;
import com.badlogic.gdx.graphics.OrthographicCamera;
import com.badlogic.gdx.graphics.Pixmap;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.BitmapFont;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.badlogic.gdx.graphics.g3d.Shader;
import com.badlogic.gdx.graphics.glutils.FrameBuffer;
import com.badlogic.gdx.graphics.glutils.ShaderProgram;
import com.badlogic.gdx.graphics.glutils.ShapeRenderer;
import com.badlogic.gdx.math.Vector3;
import com.badlogic.gdx.utils.Array;
import com.badlogic.gdx.utils.Json;
import com.badlogic.gdx.utils.viewport.ExtendViewport;
import com.badlogic.gdx.utils.viewport.ScreenViewport;
import com.badlogic.gdx.utils.viewport.StretchViewport;
import com.badlogic.gdx.utils.viewport.Viewport;

/**
 * Created by Connor on 1/21/2017.
 */
public class MainGame implements Screen{

    @Override
    public void resize(int width, int height) {
        viewport.update(width, height);
    }

    @Override
    public void pause() {

    }

    @Override
    public void resume() {

    }

    @Override
    public void hide() {

    }

    @Override
    public void dispose() {

    }
    public SwingBox swing;
    SpriteBatch batch;
    public static final float WORLD_WIDTH = 800;
    public static final float WORLD_HEIGHT = 600;
    public static final float EDITOR_CAMERA_SPEED = 200;
    public static final int TILE_SIZE = 20;
    OrthographicCamera camera;
    public static Viewport viewport;
    public static World world;
    public Texture worldSheet;
    public static TextureRegion[][] splitSheet;
    public static TextureRegion[][] miscSheet;
    public static Json json;
    public static Editor editor;
    private ShapeRenderer shapes;
    public static FileHandle m;
    public static FileHandle n;
    public static String nextLevel;
    public static BitmapFont font;
    public Music music;
    private boolean renderEditor;

    public static boolean vibrate;
    public static boolean drumming;
    private float physicsTime;

    public FrameBuffer fbo;
    public static float stringTime;
    public static float drumTime;



    public static ShaderProgram sinShader;
    public static ShaderProgram defaultShader;

    public static TextureRegion[][] itemSheet;
    public static TextureRegion[][] waveSheet;

    public Array<Texture> backgrounds;
    public static int b;


    @Override
    public void show () {
        //THIS IS FOR KEN
        b = 0;
        backgrounds = new Array<Texture>();
        backgrounds.add(new Texture(Gdx.files.internal("forestbackground.jpg")));
        backgrounds.add(new Texture(Gdx.files.internal("gasstation.jpg")));
        backgrounds.add(new Texture(Gdx.files.internal("box-truck.jpg")));
        backgrounds.add(new Texture(Gdx.files.internal("candyactory.jpg")));
        backgrounds.add(new Texture(Gdx.files.internal("sewerbackground.jpg")));
        backgrounds.add(new Texture(Gdx.files.internal("beachbackground.png")));
        backgrounds.add(new Texture(Gdx.files.internal("skybackground.png")));
        backgrounds.add(new Texture(Gdx.files.internal("moonsurface.png")));
        backgrounds.add(new Texture(Gdx.files.internal("milkywaybackground.png")));
        backgrounds.add(new Texture(Gdx.files.internal("apartmentbackground.png")));

        itemSheet = new TextureRegion(new Texture(Gdx.files.internal("items.png"))).split(40,40);
        waveSheet = new TextureRegion(new Texture(Gdx.files.internal("waves.png"))).split(40,40);
        drumTime = 0;
        swing = new SwingBox();
//        music = Gdx.audio.newMusic(Gdx.files.internal("_GameJam_2_.wav"));
//        music.setLooping(true);
//        music.play();
        drumming = false;
        vibrate = false;
        stringTime = 0;
        defaultShader = new ShaderProgram(Gdx.files.internal("Vertex.vsh").readString(), Gdx.files.internal("Fragment.fsh").readString());
        sinShader = new ShaderProgram(Gdx.files.internal("SinWave.vsh").readString(), Gdx.files.internal("Fragment.fsh").readString());
        physicsTime = 0;
        fbo = new FrameBuffer(Pixmap.Format.RGB565, 800/2, 600/2, false);
        //music = Gdx.audio.newMusic(Gdx.files.internal("Marimbrence.wav"));
        //music.setLooping(true);
        //music.setVolume((float).5);
        //music.play();
        font = new BitmapFont();
        ShaderProgram.pedantic = false;
        camera = new OrthographicCamera(WORLD_WIDTH, WORLD_HEIGHT);
        batch = new SpriteBatch();
        viewport = new StretchViewport(800,600, camera);
        camera.position.set(WORLD_WIDTH / 2, WORLD_HEIGHT / 2, 0);
        camera.update();
        shapes = new ShapeRenderer();
        worldSheet = new Texture("spritesheet.png");
        splitSheet = TextureRegion.split(worldSheet, TILE_SIZE, TILE_SIZE);
        //miscSheet = TextureRegion.split(new Texture("Misc Sprite Sheet.png"), TILE_SIZE, TILE_SIZE);
        json = new Json();
        m = Gdx.files.internal("StaticWorld/level.json");
        if(m.exists())
            world = json.fromJson(World.class, m);
        else
            world = new World();


        System.out.println(sinShader.getLog());

    }

    private void updatePhysics(float delta){
        stringTime+=delta*5;
        drumTime += 2*delta;
        physicsTime += delta;
        while(physicsTime > 1.0f/60.0f){
            stringTime+=1/60f;
            drumTime += 1/60f;
            pollInput();
            world.updatePhysics();
            physicsTime-=1.0f/60f;
            if(editor != null) {
                Vector3 mousepos = new Vector3(Gdx.input.getX(), Gdx.input.getY(), 0);
                mousepos = viewport.getCamera().unproject(mousepos);
                if(mousepos.x < 0)
                    mousepos.x-=editor.gridSize;
                if(mousepos.y < 0)
                    mousepos.y-=editor.gridSize;
                editor.setMouse(mousepos);
                renderEditor = true;
            }

            else{
                //panCamera(delta);
            }

        }

        if(editor == null) {
//            if (Gdx.input.isKeyJustPressed(Input.Keys.C))
//                editor = new Editor(world);
//            if(Gdx.input.isKeyJustPressed(Input.Keys.I)){
//                if(vibrate) {
//                    vibrate = false;
//                    for(Tile t : world.background){
//                        if(t instanceof StringCapTile){
//                            ((StringCapTile) t).activateString(world, true);
//                        }
//                    }
//                }
//                else{
//                    vibrate = true;
//                    for(Tile t : world.background){
//                        if(t instanceof StringCapTile){
//                            ((StringCapTile) t).activateString(world, false);
//                        }
//                    }
//                }
//                for(Tile t : world.background){
//                    if(t instanceof StringCapTile){
//                        ((StringCapTile) t).activateString(world, vibrate);
//                    }
//                }
//            }
            if(Gdx.input.isKeyJustPressed(Input.Keys.X))
                world.player.interact(world);
            if (Gdx.input.isKeyJustPressed(Input.Keys.Z))
                world.player.jump(1.0f/60.0f);
        }
        else{
        if(Gdx.input.isKeyJustPressed(Input.Keys.C)) {
            editor = null;
            shapes = new ShapeRenderer();
            camera.update();
            renderEditor = false;
        }

            if(Gdx.input.isKeyJustPressed(Input.Keys.E)){
                if(editor.gridSize == 32){
                    editor.gridSize = 64;
                }
                else
                    editor.gridSize = 32;
            }
            if(Gdx.input.isKeyJustPressed(Input.Keys.L)){
                Gdx.input.getTextInput(swing, "filename x y", "", "");
            }
            if(Gdx.input.isKeyJustPressed(Input.Keys.K))
                editor.setDoorLevel();
            if(Gdx.input.isKeyPressed(Input.Keys.BACKSPACE))
                editor.clearTile();
            if(Gdx.input.isKeyPressed(Input.Keys.UP)){
                viewport.getCamera().position.add(0,EDITOR_CAMERA_SPEED*1.0f/60.0f, 0);
                viewport.getCamera().update();
            }
            if(Gdx.input.isKeyPressed(Input.Keys.DOWN)){
                viewport.getCamera().position.add(0,-EDITOR_CAMERA_SPEED*1.0f/60.0f, 0);
                viewport.getCamera().update();
            }
            if(Gdx.input.isKeyPressed(Input.Keys.LEFT)){
                viewport.getCamera().position.add(-EDITOR_CAMERA_SPEED*1.0f/60.0f, 0, 0);
                viewport.getCamera().update();
            }
            if(Gdx.input.isKeyPressed(Input.Keys.RIGHT)){
                viewport.getCamera().position.add(EDITOR_CAMERA_SPEED*1.0f/60.0f, 0, 0);
                viewport.getCamera().update();
            }
            if(Gdx.input.isButtonPressed(Input.Buttons.LEFT)){
                editor.addForeground();
            }
            if(Gdx.input.isButtonPressed(Input.Buttons.RIGHT) || Gdx.input.isKeyPressed(Input.Keys.B)){
                editor.addBackground();
            }
            if(Gdx.input.isKeyJustPressed(Input.Keys.RIGHT_BRACKET))
                editor.removeForeground();

            if(Gdx.input.isKeyJustPressed(Input.Keys.PERIOD)) {
                editor.scroll(1);
            }
            if(Gdx.input.isKeyJustPressed(Input.Keys.COMMA))
                editor.scroll(-1);

            if(Gdx.input.isKeyJustPressed(Input.Keys.SHIFT_LEFT))
                editor.flipx();
            if(Gdx.input.isKeyJustPressed(Input.Keys.CONTROL_LEFT))
                editor.flipy();
        }
    }

    public static void activateStrings(){

        if(vibrate) {
            vibrate = false;
            for(Tile t : world.background){
                if(t instanceof StringCapTile){
                    ((StringCapTile) t).activateString(world, true);
                }
            }
        }
        else{
            vibrate = true;
            for(Tile t : world.background){
                if(t instanceof StringCapTile){
                    ((StringCapTile) t).activateString(world, false);
                }
            }
        }
        for(Tile t : world.background){
            if(t instanceof StringCapTile){
                ((StringCapTile) t).activateString(world, vibrate);
            }
        }
    }

    public static void activateDrums(){
        for(Tile t : world.background){
                if(t instanceof Drum){
                    ((Drum) t).activate();

        }
    }
    }

    @Override
    public void render (float delta) {
        //fbo.begin();
        renderEditor = false;
        Gdx.gl.glClearColor(0, 0, 0, 1);
        Gdx.gl.glClear(GL20.GL_COLOR_BUFFER_BIT);
        updatePhysics(delta);
        batch.setProjectionMatrix(viewport.getCamera().combined);
        shapes.setProjectionMatrix(viewport.getCamera().combined);
        batch.begin();
        batch.draw(backgrounds.get(b),0,0, 800, 600);
        world.render(batch, delta);
        batch.end();
//        fbo.end();
//        batch.begin();
//        batch.draw(fbo.getColorBufferTexture() ,camera.position.x-camera.viewportWidth/2, camera.position.y-camera.viewportHeight/2, WORLD_WIDTH, WORLD_HEIGHT, 0, 0, 1, 1);
//        batch.end();
        if(renderEditor)
            editor.render(batch, shapes, viewport.getCamera().position.x - viewport.getWorldWidth() / 2, viewport.getCamera().position.y - viewport.getWorldHeight() / 2);

        if(b>9){
            ((Game)Gdx.app.getApplicationListener()).setScreen(new MainMenu());
        }


    }

    public void pollInput(){
        if(editor == null) {
            if(Gdx.input.isKeyJustPressed(Input.Keys.L)) {
                world.player.kill();
            }


            if ( Gdx.input.isKeyPressed(Input.Keys.RIGHT))
                world.player.walk(1.0f/60.0f);
            if (Gdx.input.isKeyPressed(Input.Keys.LEFT))
                world.player.walk(-1.0f/60.0f);

            if (Gdx.input.isKeyPressed(Input.Keys.Z))
                world.player.flaot(1.0f/60.0f);

        }
        else{
            if(Gdx.input.isKeyJustPressed(Input.Keys.G)){
                editor.changeGridSize();
            }
        }
    }

    public void panCamera(float delta){
        if(world.player.x + world.player.width >= camera.position.x + viewport.getWorldWidth()/5)
            camera.position.x += world.player.x + world.player.width - (camera.position.x + viewport.getWorldWidth()/5);

        if(world.player.x <= camera.position.x - viewport.getWorldWidth()/5)
            camera.position.x += world.player.x - (camera.position.x - viewport.getWorldWidth()/5);

        if(world.player.y + world.player.height > camera.position.y + viewport.getWorldHeight()/5) {
            camera.position.y += world.player.y + world.player.height - (camera.position.y + viewport.getWorldHeight()/5);
        }
        if(world.player.y  < camera.position.y - viewport.getWorldHeight()/4) {
            camera.position.y += world.player.y - (camera.position.y - viewport.getWorldHeight()/4);
        }
        camera.update();
    }


}
