package com.mygdx.game;

/**
 * Created by hcmg_local on 1/22/2017.
 */
public class DoubleSwitch extends StringSwitch{
    public DoubleSwitch(int x, int y, int sx, int sy, boolean fx, boolean fy){
        super(x,y,sx,sy,fx,fy);
    }

    public DoubleSwitch(){
        super();
    }

    public DoubleSwitch clone(int x, int y, boolean fx, boolean fy){
        return new DoubleSwitch(x,y,sx,sy,fx,fy);
    }

    public void interact(){
        MainGame.activateStrings();
        MainGame.stringTime = 0;
        MainGame.activateDrums();
    }
}
