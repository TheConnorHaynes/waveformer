package com.mygdx.game;

import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.utils.Array;
import com.badlogic.gdx.utils.Json;
import com.badlogic.gdx.utils.JsonValue;

/**
 * Created by Connor on 5/10/2016.
 */
public class MovingObject implements Json.Serializable {
    protected float x;
    protected float y;
    protected float dx;
    protected float dy;
    float speed;
    protected float height;
    protected float width;
    protected int element;
    boolean flipped;
    boolean grounded;


    float grav;
    float maxSpeed;
    final float JUMP = 5.5f;
    final float FLOAT = 17;
    final float TERMINAL_VELOCITY = -6;
    float friction;
    final float ACCELERATION = 1400;
    float groundTime;
    final float MAX_GROUND_TIME = .1f;


    public MovingObject(float x, float y, float width, float height){
        groundTime = MAX_GROUND_TIME;
        this.x = x;
        this.y = y;
        this.width = width;
        this. height = height;
        dx = 0;
        dy = 0;
        friction = 1000;
        maxSpeed = 100;
        grav = -40;
    }

    public void updatePhysics(){
        dx = speed*1f/60f;
        checkGround(MainGame.world.foreground);
        move(MainGame.world.foreground);
        if(dy > TERMINAL_VELOCITY)
            dy += grav/60f;
        if(speed > 0){
            speed -= friction/60f;
            if(speed < 0)
                speed = 0;
        }
        if(speed < 0){
            speed += friction/60f;
            if(speed > 0)
                speed = 0;
        }
    }

    public MovingObject(){
        dx = 0;
        dy = 0;
        maxSpeed = 100;
        friction = 500;
        grav = -25;
    }

    public void walk(float walk){
        speed += walk*ACCELERATION;

        if(speed > maxSpeed)
            speed = maxSpeed;
        if(speed < -maxSpeed)
            speed = -maxSpeed;
    }

    public void move(Array<Tile> tiles){
        x+=dx;
        for(Tile t : tiles){
            x -= t.collidex(this);
        }

        y+=dy;
        for(Tile t : tiles){
            y -= t.collidey(this);

        }
    }



    public boolean equals(Object o){
        boolean ret = false;
        if(o instanceof MovingObject) {
            if (((MovingObject) o).x == x && ((MovingObject) o).y == y)
                ret = true;
        }
        return ret;
    }

    public void kill(){

    }

    public void render(SpriteBatch batch, float delta, World world){

    }


    public void checkGround(Array<Tile> tiles){
        grounded = false;
        if(groundTime >0)
            groundTime-= 1/60f;
        y-=1;
        for(Tile tile : tiles){
            if(tile.collidey(this) != 0) {
                grounded = true;
                groundTime = MAX_GROUND_TIME;
            }
        }
        y+=1;


        x -= 1;
        x += 2;
        x -= 1;

    }

    @Override
    public void write(Json json) {
        json.writeValue("x", x);
        json.writeValue("y", y);
        json.writeValue("width", width);
        json.writeValue("height", height);
    }

    @Override
    public void read(Json json, JsonValue jsonData) {
        x = jsonData.getFloat("x");
        y = jsonData.getFloat("y");
        width = jsonData.getFloat("width");
        height = jsonData.getFloat("height");
    }

    public void jump(float delta){
        if(groundTime>0 ) {
            dy = JUMP;
            groundTime = 0;
        }
    }

    public void flaot(float delta){
        dy += FLOAT*delta;
    }

    public boolean collides(MovingObject o){
        return (x < o.x+o.width && x+width > o.x && y < o.y+o.height && y+height > o.y);
    }
}
