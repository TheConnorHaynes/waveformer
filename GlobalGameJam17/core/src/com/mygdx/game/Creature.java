package com.mygdx.game;

import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.Animation;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.utils.Array;

/**
 * Created by Connor on 5/29/2016.
 */
public class Creature extends MovingObject {

    public float weight;
    public Animation animation;
    public float animEnd;
    public float time;
    public float health;

    public TextureRegion[][] spriteSheet;

    public Creature(float x, float y, float width, float height){
        super(x, y, width, height);
        speed = 0;
        weight = 10;
        health = 10;
        animation = null;
        animEnd = 10;
        time = 0;
    }

    public void render(SpriteBatch batch, float delta, World world){
        batch.draw(animation.getKeyFrame(time), x, y);
        super.move(world.foreground);

        time+=delta;
        if(time>=animEnd)
            time = 0;
        super.render(batch, delta, world);
    }

    public void hurt(int dmg, boolean knockback){
        if(health <= 0){
            kill();
        }
    }

}
