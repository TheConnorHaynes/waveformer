package com.mygdx.game;

import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.utils.Array;
import com.badlogic.gdx.utils.Json;
import com.badlogic.gdx.utils.JsonValue;

/**
 * Created by hcmg_local on 1/21/2017.
 */
public class Drum extends Tile {
    boolean drumming;
    boolean hasTiles;
    boolean stopping;
    float stopPoint;
    Array<Tile> area;
    public Drum(int x, int y, int sx, int sy, boolean fx, boolean fy){
        super(x,y,sx,sy,fx,fy);
        drumming = false;
        hasTiles = false;
        stopping = false;
        stopPoint = 0;
        area = new Array<Tile>();
        texture = MainGame.waveSheet[0][0];
    }

    public Drum(){
        super();
        drumming = false;
        hasTiles = false;
        stopping = false;
        area = new Array<Tile>();
        stopPoint = 0;
        texture = MainGame.waveSheet[0][0];
    }

    public void getTiles(){
        for(Tile t : MainGame.world.foreground){
            if(Math.sqrt(Math.pow(x+width/2-t.x+t.width/2,2)+Math.pow(y+height/2-t.y+t.height/2, 2)) <= 90){
                area.add(t);
            }
        }
    }

    public void activate(){
        if(!drumming) {
            System.out.println("drum1");
            getTiles();
            drumming = true;
            MainGame.drumTime = 0;
        }
        else {
            drumming = false;
            stopping = true;
            stopPoint = ((int)MainGame.drumTime*10000/(int)(2*Math.PI*1000)+(int)(2*Math.PI*1000))/1000f;
        }
    }

    public void render(SpriteBatch batch, float delta, World world){
        super.render(batch, delta, world);
        if(!hasTiles){
            getTiles();
            hasTiles = true;
        }

        if(drumming){
            drum();
        }
        if(stopping){
            drum();
            if(MainGame.drumTime>stopPoint)
                stopping = false;
        }
    }

    @Override
    public void read(Json json, JsonValue jsonData){
        super.read(json, jsonData);
        texture = MainGame.waveSheet[0][0];
    }

    public void drum(){
//        if(Math.sqrt(Math.pow(MainGame.world.player.x+MainGame.world.player.width/2 - x, 2) + Math.pow(MainGame.world.player.y+MainGame.world.player.height/2 - y, 2))<100+100*Math.sin(MainGame.drumTime)){
//            if(MainGame.world.player.grounded){
//                if(MainGame.world.player.x+MainGame.world.player.width/2 > x+width)
//                    MainGame.world.player.x += 5*Math.sin(MainGame.drumTime);
//                if(MainGame.world.player.x+MainGame.world.player.width/2 < x)
//                    MainGame.world.player.x -= 5*Math.sin(MainGame.drumTime);
//
//                if(MainGame.world.player.y+MainGame.world.player.height/2 > y+height)
//                    MainGame.world.player.y += 5*Math.sin(MainGame.drumTime);
//                if(MainGame.world.player.y+MainGame.world.player.height/2 < y)
//                    MainGame.world.player.y -= 5*Math.sin(MainGame.drumTime);
//            }
//        }
        for(Tile t : area){
            if(t.x+t.width/2 >= x+width)
                t.x += Math.sin(MainGame.drumTime);
            if(t.x+t.width/2 < x)
                t.x -= Math.sin(MainGame.drumTime);

            if(t.y+t.height/2 > y+height)
                t.y += Math.sin(MainGame.drumTime);
            if(t.y+t.height/2 < y)
                t.y -= Math.sin(MainGame.drumTime);
        }

    }

    public Drum clone(int x, int y, boolean fx, boolean fy){
        return new Drum(x, y, sx, sy, fx, fy);
    }
}
